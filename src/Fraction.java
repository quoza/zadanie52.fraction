public class Fraction {
    private int licznik;
    private int mianownik;
    private double ulamek;

    Fraction(int licznik, int mianownik) {
        this.licznik = licznik;
        this.mianownik = mianownik;
        correction();
        this.ulamek = licznik / mianownik;
    }

    public Fraction() {
        this.ulamek = 0;
    }

    public Fraction(int licznik) {
        this.licznik = licznik;
        this.ulamek = licznik;
    }

    public Fraction(Fraction fraction) {
        this.licznik = fraction.licznik;
        this.mianownik = fraction.mianownik;
        this.ulamek = fraction.ulamek;
    }

    public int getLicznik() {
        return licznik;
    }

    public void setLicznik(int licznik) {
        this.licznik = licznik;
    }

    public int getMianownik() {
        return mianownik;
    }

    public void setMianownik(int mianownik) {
        this.mianownik = mianownik;
    }

    public double getUlamek() {
        return ulamek;
    }

    public void setUlamek(double ulamek) {
        this.ulamek = ulamek;
    }

    private void correction() {
        if (this.licznik < 0 && this.mianownik < 0) {
            this.licznik = -licznik;
            this.mianownik = -mianownik;
        } else if (this.licznik > 0 && this.mianownik < 0) {
            this.licznik = -licznik;
            this.mianownik = -mianownik;
        }
    }

    void reduce() {
        int gcd = MathUtil.nwd(licznik, mianownik);
        this.licznik = licznik / gcd;
        this.mianownik = mianownik / gcd;
    }

    void extend(int extendingValue) {
        this.licznik *= extendingValue;
        this.mianownik *= extendingValue;
    }

    Fraction multiply(Fraction ulamek1) {
        int licznik2 = ulamek1.licznik * this.licznik;
        int mianownik2 = ulamek1.mianownik * this.mianownik;
        return new Fraction(licznik2, mianownik2);
    }

    Fraction multiply(Integer mnoznik){
        return new Fraction(this.licznik * mnoznik, this.mianownik);
    }

    Fraction reverse(){
        return new Fraction(this.mianownik, this.licznik);
    }

    Fraction divide(Fraction ulamek2){
        ulamek2 = ulamek2.reverse();
        return this.multiply(ulamek2);
    }

    Fraction add(Fraction ulamek2){
        int nowyMianownik = MathUtil.nww(this.mianownik, ulamek2.mianownik);
        int counter1 = nowyMianownik / this.mianownik;
        int counter2 = nowyMianownik / ulamek2.mianownik;
        int nowyLicznik = this.licznik * counter1 + ulamek2.licznik * counter2;
        return new Fraction(nowyLicznik, nowyMianownik);
    }

    Fraction subtract(Fraction ulamek2){
        int nowyMianownik = MathUtil.nww(this.mianownik, ulamek2.mianownik);
        int counter1 = nowyMianownik / this.mianownik;
        int counter2 = nowyMianownik / ulamek2.mianownik;
        int nowyLicznik = this.licznik * counter1 - ulamek2.licznik * counter2;
        return new Fraction(nowyLicznik, nowyMianownik);
    }

    @Override
    public String toString() {
        if(licznik != mianownik) {
            return licznik + "/" + mianownik;
        } else {
            return licznik + "";
        }
    }

    boolean equals(Fraction fraction) {
        this.reduce();
        fraction.correction();
        fraction.reduce();
        return this.licznik == fraction.licznik && this.mianownik == fraction.mianownik;
    }
}



//  p) Metoda Object.equals() jest dziedziczona przez wszystkie klasy i pozwala ustali,
//        czy dwa obiekty są identyczne. W klasie Fraction za równe uznamy te obiekty,
//        które reprezentują ten sam ułamek. Utwórz i dołącz do klasy metodę equals(),
//        przesłaniającą metodę Object.equals() , oraz napisz program testujący poprawność
//        działania zbudowanej metody