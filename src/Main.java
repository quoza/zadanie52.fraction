import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Program Fraction");
        System.out.println("Podaj licznik (int):");
        int licznik = sc.nextInt();
        System.out.println("Podaj mianownik (int):");
        int mianownik = sc.nextInt();
        Fraction ulamek = new Fraction(licznik, mianownik);
        System.out.println("Ulamek stworzony.");
        String command;
        System.out.println("Podaj komende (print, reduce, extend, equals, add, subtract, multiply, divide, quit): ");
        do{
            command = sc.nextLine();
            if(command.equals("quit")){break;}
            else if(command.equals("reduce")){
                ulamek.reduce();
                System.out.println(ulamek);
            } else if (command.equals("extend")){
                System.out.println("Podaj mnoznik.");
                int mnoznik = sc.nextInt();
                ulamek.extend(mnoznik);
                System.out.println(ulamek);
            } else if (command.equals("print")){
                System.out.println(ulamek.toString());
            } else if(command.equals("divide")){
                System.out.println("Podaj drugi ulamek przez ktory chcesz podzielic.");
                System.out.println("Podaj licznik: ");
                int licznik2 = sc.nextInt();
                System.out.println("Podaj mianownik: ");
                int mianownik2 = sc.nextInt();
                Fraction ulamek2 = new Fraction(licznik2, mianownik2);
                ulamek = ulamek.divide(ulamek2);
                ulamek.reduce();
                System.out.println(ulamek);
            } else if(command.equals("multiply")){
                System.out.println("Podaj drugi ulamek przez ktory chcesz pomnozyc.");
                System.out.println("Podaj licznik: ");
                int licznik2 = sc.nextInt();
                System.out.println("Podaj mianownik: ");
                int mianownik2 = sc.nextInt();
                Fraction ulamek2 = new Fraction(licznik2, mianownik2);
                ulamek = ulamek.multiply(ulamek2);
                System.out.println(ulamek);
            } else if(command.equals("add")){
                System.out.println("Podaj drugi ulamek ktory chcesz dodac.");
                System.out.println("Podaj licznik.");
                int licznik2 = sc.nextInt();
                System.out.println("Podaj mianownik.");
                int mianownik2 = sc.nextInt();
                Fraction ulamek2 = new Fraction(licznik2, mianownik2);
                ulamek = ulamek.add(ulamek2);
                System.out.println(ulamek);
            } else if (command.equals("subtract")){
                System.out.println("Podaj drugi ulamek ktory chcesz odjac.");
                System.out.println("Podaj licznik.");
                int licznik2 = sc.nextInt();
                System.out.println("Podaj mianownik.");
                int mianownik2 = sc.nextInt();
                Fraction ulamek2 = new Fraction(licznik2, mianownik2);
                ulamek = ulamek.subtract(ulamek2);
                System.out.println(ulamek);
            } else if (command.equals("equals")){
                System.out.println("Podaj drugi ulamek do ktorego chcesz porownac.");
                System.out.println("Podaj licznik:");
                int licznik2 = sc.nextInt();
                System.out.println("Podaj mianownik:");
                int mianownik2 = sc.nextInt();
                Fraction ulamek2 = new Fraction(licznik2, mianownik2);
                if (ulamek.equals(ulamek2)){
                    System.out.println("Ulamki sa rowne.");
                } else {
                    System.out.println("Ulamki nie sa rowne.");
                }
            }
        } while (!command.equals("quit"));
    }
}